# Installation and Test on Ubuntu 20.04

1) Install Perl and Tools

```bash
sudo apt install perl
sudo apt install snmpwalk
sudo apt install snmp
sudo apt install libsnmp-dev
sudo apt install libsnmp-perl
sudo apt install libnet-snmp-perl
sudo apt install snmp-mibs-downloader
sudo apt install libperl-dev
sudo apt install libc6-dev
```
2) Configure MIBs

Download the MIBs:

One of the default lookups for `SwitchMap` is `~/snmp/mibs`.
So, let's fill it with needed MIBs:

```bash
mkdir -p ~/.snmp/mibs
cd ~/.snmp/mibs
wget https://github.com/netdisco/netdisco-mibs/releases/download/4.020/netdisco-mibs.tar.gz
tar -xzf netdisco-mibs.tar.gz
cp netdisco-mibs-4.020/cisco/* .
cp netdisco-mibs-4.020/ciscosb/* .
cp netdisco-mibs-4.020/rfc/* .
rm -rf netdisco-mibs-*
```

3) Install
[cpanminus](https://www.cpan.org/modules/INSTALL.html)

```bash
cpan App::cpanminus
```

Make sure the local perl5Dir is in your $PATH
This should happen automatically during the installation of cpanminus.

```bash
cpanm Log::Log4perl
cpanm Module::Build
cpanm Log::Dispatch::Screen
cpanm SNMP::Info
```

4) Install snmpsim


```bash
git clone https://github.com/etingof/snmpsim.git
```

5) Start snmpsim

```bash
cd snmpsim
sudo snmpsimd --data-dir=data --agent-udpv4-endpoint=127.0.0.1 --process-user=nobody --process-group=nogroup
```

Start `snmpsimd` as User `nobody` and group `nogroup` Port `161`which is the Cisco standardport.

**Is absolutely necessary**, since `SwitchMap.pl` relies on using the standard port

6) Test working simulation

```bash
snmpwalk -v1 -c public 127.0.0.1:161
```

7) Install SwitchMap

```bash
git clone https://gitlab.com/sascha.l.teichmann/switchmap/
```

8) Configuration of `ThisSite.pm` in the SwitchMap Folder

Comment out all  ```push``` commands, which are used to fill the list variables

e.g.

```Perl
#push @LocalSwitches, '---Building1';
#push @LocalSwitches, 'switch1-in-building1.abc.com';
#push @LocalSwitches, 'switch2-in-building1.abc.com';
#push @LocalSwitches, '---Building2';
#push @LocalSwitches, 'switch1-in-building2.abc.com';
#push @LocalSwitches, 'switch2-in-building2.abc.com';
```


Example config

```Perl
...
@routers = ();
...
@LocalSwitches = ('127.0.0.1')
...
$DnsDomain = 'localhost';
...
$DestinationDirectory '/home/thomas/portlists';
...
$DestinationDirectoryRoot = '/home/thomas/portlists';
...
$StateFileDirectory = '/home/thomas/portlists';
```

8) Run Switchmap

```bash
#assuming you are in Switchmap Folder

./GetArp.pl #creates MacList under $DestinationDirectory
./ScanSwitch.pl # creates idlesince under $DestinationDirectory
./SwitchMap.pl # creates the devicemap under $DestinationDirectory as HTML
```
